/**
 * Created by Pradnya on 01/23/2015.
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'Model/sitelocation',
    'Model/selflocation',
    'text!../../templates/locationInfo.html'
], function($, _, Backbone,sitelocation,selflocation, locationInfoTemplate){
    var selfLocationView = Backbone.View.extend({
        template:locationInfoTemplate,
        
    initialize:function(model){
        this.model = model;
        this.mapView = new sitelocation({model: this.model.attributes});
        this.userLocation=[];
    },
    render: function() {
        var data = this.model.attributes;
        this.template = _.template(locationInfoTemplate);        
        this.$el.append( this.template(data));
    },
    resetLocationDetails:function(){        
        this.updateLocationDetails({
            query: "0.0.0.0",
            country: "",
            regionName: "",
            city: "",
            timezone: "",
            lat: "",
            lon: ""
        });
        $("table").addClass("empty");        
    },
    updateLocationDetails:function(data){
        var now = new Date();
        $("#location_query").html(data.query);
        $("#location_country").html(data.country);
        $("#location_regionName").html(data.regionName);
        $("#location_city").html(data.city);
        $("#location_timezone").html(data.timezone);
        $("#location_lat").html(data.lat);
        $("#location_lon").html(data.lon);
        $("table").removeClass("empty");
        $(".help").click(function(e){
            var fieldName = $(e.currentTarget).closest('tr').find('.field_name').text();
            alert("This is your " + fieldName + " from ISP " + data.isp + " at " + now);
        });
    },
    getlocationDetails:function(response){   
       this.updateLocationDetails(response);     
       this.userLocation.push({
            lat: response.lat,
            lon: response.lon
        });
    }
    });
    return selfLocationView;
});