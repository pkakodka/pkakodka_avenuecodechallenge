/**
 * Created by Pradnya on 01/23/2015.
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'Model/sitelocation',
    'Model/selflocation',
    'Views/selflocation',
    'Views/sitelocation',
    'text!../../templates/parent.html'
], function($, _, Backbone, sitelocation, selflocation, selfLocationView, siteLocationView, ParentTemplate) {
    var parentView = Backbone.View.extend({
        events: {
            'click #showLocation': 'showMap',
            'click #btnMyLocation': 'getlocationDetails',
            'click #btnResetLocation': 'resetLocationDetails'
        },
        initialize: function() {
            this.siteLocationModel = new sitelocation();
            this.selfLocationModel = new selflocation();
            this.selfLocationView = new selfLocationView(this.selfLocationModel);
            this.siteLocationView = new siteLocationView(this.siteLocationModel);
        },
        render: function() {
            this.template = _.template(ParentTemplate);
            this.$el.append(this.template());
            this.selfLocationView.render();
            $('#selfContainer').append(this.selfLocationView.$el);
            this.siteLocationView.render();
            $('#siteContainer').append(this.siteLocationView.$el);
        },
        update_location: function(options) {
            console.log(options);
        },
        showMap: function() {
            var hostName = this.$("#hostName").val();
            if(!this.validURL(hostName)){
                this.$("#hostName").addClass("error");
                return;
            }
            this.$("#hostName").removeClass("error");
            this.siteLocationView.showMap(hostName);
        },
        getlocationDetails: function() {
            $.ajax({
                type: 'GET',
                url: 'http://ip-api.com/json/'
            }).done(function(response) {
                this.selfLocationView.getlocationDetails(response);
                this.siteLocationView.selfLocation = {
                    lat: response.lat,
                    lon: response.lon
                };
                this.siteLocationView.renderMap();
            }.bind(this)).fail(function(jqXHR, status) {});

        },
        resetLocationDetails: function() {
            this.selfLocationView.resetLocationDetails();
            this.siteLocationView.selfLocation = null;
            this.siteLocationView.renderMap();
        },
        validURL: function(url) {
            return url.match(/([?:www\.](?:[-a-z0-9]+\.)*[-a-z0-9]+.*)/i);              
        }

    });
    return parentView;
});
