/**
 * Created by Pradnya on 01/23/2015.
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'Model/sitelocation',
    'Model/selflocation',
    'text!../../templates/mapView.html'
], function($, _, Backbone,sitelocation,selflocation, MapTemplate){
    var siteLocationView = Backbone.View.extend({
        template:MapTemplate,
        
    initialize:function(model){
        this.model = model;
        this.removeMarker=false;
        this.eventAggregator.bind('showMap', this.renderMap, this);
    },
    render: function() {
        var data = {};
        var compiledTemplate = _.template( MapTemplate, data );
        this.$el.append( compiledTemplate );
    },
    showMap:function(hostName){
        var renderMapBound = this.renderMap.bind(this);
        var host = hostName;
        this.model.set("hostName", host);
        this.model.fetch({
                success: renderMapBound, 
                error: (function (e) {
                        alert("Unable to finding website location");
                    })
            });
    },
    renderMap: function(){
        var siteLatlng = new google.maps.LatLng(this.model.get("latitude"),this.model.get("longitude"));
        var mapOptions = {
            zoom: 4,
            center: siteLatlng
        };
        if(!this.model.get("latitude")){
            return;
        }
        var map = new google.maps.Map(this.$("#map-canvas")[0], mapOptions);
        var marker = new google.maps.Marker({
            position: siteLatlng,
            map: map
        });
        if(this.selfLocation) {
            var selfLatlng = new google.maps.LatLng(this.selfLocation.lat, this.selfLocation.lon);
            var marker2 = new google.maps.Marker({
                position: selfLatlng,
                map: map
            });
            var latlngbounds = new google.maps.LatLngBounds();
            latlngbounds.extend(selfLatlng);
            latlngbounds.extend(siteLatlng);
            map.setCenter(latlngbounds.getCenter());
            map.fitBounds(latlngbounds);
            if(this.removeMarker){
                marker2.setMap(null);
            }
        }
    },
    renderSelfMarker: function(options){
        this.selfLocation = options;
        this.renderMap();
    },
    removeSelfMarker: function(options){
        this.selfLocation = options;
        this.removeMarker = true;
        this.renderMap();
    }
    });
    return siteLocationView;
});