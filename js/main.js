/**
 * Created by Pradnya on 01/23/2015.
 */
require.config({
    paths: {
        "jquery": 'libs/jquery/jquery',
        "underscore": 'libs/underscore/underscore',
        "backbone": 'libs/backbone/backbone',
        "app":"app"     
    }
});
require(["app"], function(App){
	App.initialize();
});