/**
 * Created by Pradnya on 01/23/2015.
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'Views/parent'    
], function($, _, Backbone, ParentView){
    var AppRouter = Backbone.Router.extend({
        routes: {
            '*actions': 'defaultAction'            
        }
    });
    var initialize = function(){
        Backbone.View.prototype.eventAggregator = _.extend({}, Backbone.Events);
        var app_router = new AppRouter();
        var parentView = new ParentView({el: $("#parentAction")});        
        
        app_router.on('route:defaultAction', function(actions){        
            parentView.render();
        }); 
        
        Backbone.history.start();
    };
    return {
        initialize: initialize
    };
});

