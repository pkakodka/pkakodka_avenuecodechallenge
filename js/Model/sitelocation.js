/**
 * Created by Pradnya on 01/23/2015.
 */
define([
    'underscore',
    'backbone'
], function (_, Backbone) {
    var siteLocationModel = Backbone.Model.extend({
        urlRoot: "http://freegeoip.net/json/",
        url: function () {
            return this.urlRoot + this.attributes.hostName;
        }
    });
    return siteLocationModel;
});