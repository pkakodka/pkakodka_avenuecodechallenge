/**
 * Created by Pradnya on 01/23/2015.
 */
define([
    'underscore',
    'backbone'
], function (_, Backbone) {
    var selfLocationModel = Backbone.Model.extend({
        defaults: {
            query: "0.0.0.0",
            country: "",
            regionName: "",
            city: "",
            timezone: "",
            lat: "",
            lon: ""
        }
    });
    return selfLocationModel;
});