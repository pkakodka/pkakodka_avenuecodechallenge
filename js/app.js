/**
 * Created by Pradnya on 01/23/2015.
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'router'
], function($, _, Backbone, Router){
    var initialize = function(){
        // Pass in our Router module and call it's initialize function
        Router.initialize();
    };
    return {
        initialize: initialize
    };
});